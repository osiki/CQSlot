#ifndef CQ_TASK_POLICY_H
#define CQ_TASK_POLICY_H

#include "CQ_defines.h"
#include "CQ_tasktype.h"

DEFINE_NAMESPACE_CQ_BEGIN

typedef std::chrono::milliseconds ms;

struct CQTaskPolicy
{
	virtual ~CQTaskPolicy() {}
	virtual ms ttl() const
	{
		return ms(0);
	}

	virtual int type() const = 0;

	virtual void setTtl(ms ms_)
	{
		CQ_UNUSE(ms_);
	}
};

class CQCommonTaskPolicy : public CQTaskPolicy
{
public:
	int type() const
	{
		return TASK_COMMON;
	}
};

class CQDelayTaskPolicy : public CQTaskPolicy
{
public:

	CQDelayTaskPolicy()
		: m_delay(std::chrono::system_clock::now())
	{

	}

	int type() const
	{
		return TASK_DELAY;
	}

	ms ttl() const
	{
		std::chrono::system_clock::duration _tt = m_delay - std::chrono::system_clock::now();
		ms _t = std::chrono::duration_cast<std::chrono::milliseconds>(_tt);
		if (_t.count() > 0) {
			return _t;
		}
		return ms(0);
	}

	void setTtl(ms ms_)
	{
		m_delay = std::chrono::system_clock::now() + ms_;
	}

private:
	std::chrono::system_clock::time_point m_delay;
};

DEFINE_NAMESPACE_CQ_END

#endif // CQ_TASK_POLICY_H
